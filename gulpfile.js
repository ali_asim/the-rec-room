// define
var gulp = require('gulp'),
		gutil = require('gulp-util'),
		connect = require('gulp-connect'),
		uglify = require('gulp-uglify'),
		concat = require('gulp-concat'),
		less = require('gulp-less'),
		lessImport = require('gulp-less-import'),
		minifyCSS = require('gulp-clean-css'),
		watch = require('gulp-watch'),
		nunjucksRender = require('gulp-nunjucks-render');


// template language
gulp.task('nunjucks', function() {
  // Gets .html and .nunjucks files in pages
  return gulp.src('pages/**/*.+(html|nunjucks)')
  // Renders template with nunjucks
  .pipe(nunjucksRender({
      path: ['templates']
    }))
  // output files in app folder
  .pipe(gulp.dest('.'))
});



// scripts task
gulp.task('scripts',function(){
	gulp.src([
				'assets/dist/js/lib/jquery-2.2.1.min.js',
				'assets/dist/js/lib/bootstrap.min.js',
				'assets/dist/js/lib/parsley.min.js',
				'assets/dist/js/lib/imagesloaded.pkgd.min.js',
				'assets/dist/js/lib/isotope.pkgd.min.js',
				'assets/dist/js/lib/jquery.mobile.min.js',
				'assets/dist/js/lib/moment.js',
				'assets/dist/js/lib/owl.carousel.min.js',
				'assets/dist/js/lib/bootstrap-slider.js',
				'assets/dist/js/lib/select2.min.js',
				'assets/dist/js/lib/bootstrap-datetimepicker.min.js',
				// vertical slider
				'assets/dist/js/lib/jquery.easing.1.3.js',
				'assets/dist/js/lib/jquery.mousewheel.min.js',
				'assets/dist/js/lib/mbVerticalSlider.js',
				'assets/js/**/*.js',
			])
	.pipe(concat('scripts.min.js'))
	//.pipe(uglify())
	.pipe(gulp.dest('assets/dist/js'))
	.pipe(connect.reload());
});
// css task
gulp.task('styles',function(){
	gulp.src('assets/css/global/*')
	.pipe(lessImport('assets/css/imports.less'))
	.pipe(less())
	.pipe(minifyCSS())
	.pipe(concat('global.css'))
	.pipe(gulp.dest('assets/dist/css'))
	.pipe(connect.reload());
});
// watcher
gulp.task('watch',function(){
	gulp.watch('**/*.+(html|nunjucks)',['nunjucks']);
	gulp.watch('assets/js/**/*',['scripts']);
	gulp.watch('assets/css/**/*',['styles']);
});



gulp.task('default', ['nunjucks', 'watch']);
