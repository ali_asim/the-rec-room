# README #

This is the rec room fronted templates repo

### How do I get set up? ###

* We are using NPM Gulp for CSS / Javascript compression
* just type - npm install and then Gulp watch to get things going
* Gulp file is included
* Less is our css pre-processor
* All the js libraries are in assets\dist\js\lib
* page-home.js is includes under assets\dist\js\
* global.js assets\js\ 
* PHP is being used specifically for Header Footer includes and for the mobile detect library

#### please contact - Ali Asim - ali.asim@cundari.com if you have any issues or concerns.