/*******************************
PROJECT : REC ROOM
JAVASCRIPT : GLOBAL JS
to Initialize & modify behaviour
********************************/
jQuery(document).ready(function () {

    // slider carousel - animations
    (function( $ ) {
    //Function to animate slider captions
    function doAnimations( elems ) {
        //Cache the animationend event in a variable
        var animEndEv = 'webkitAnimationEnd animationend';
        elems.each(function () {
            var $this = $(this),
                $animationType = $this.data('animation');
            $this.addClass($animationType).one(animEndEv, function () {
                $this.removeClass($animationType);
            });
        });
    }
    //Variables on page load
    var $homeCarousel1 = $('#slider'),
        $firstAnimatingElems = $homeCarousel1.find('.item:first').find("[data-animation ^= 'animated']");
    //Initialize carousel
    $homeCarousel1.carousel();
    //Animate captions in first slide on page load
    doAnimations($firstAnimatingElems);
    //Pause carousel
    $homeCarousel1.carousel('pause');
    //Other slides to be animated on carousel slide event
    $homeCarousel1.on('slide.bs.carousel', function (e) {
        var $animatingElems = $(e.relatedTarget).find("[data-animation ^= 'animated']");
        doAnimations($animatingElems);
    });
    $homeCarousel1.swiperight(function() {
      $homeCarousel1.carousel('prev');
    });
    $homeCarousel1.swipeleft(function() {
      $homeCarousel1.carousel('next');
    });
    })(jQuery);

    // search
    jQuery('#buttonsearch').click(function(){
        jQuery('#formsearch').slideToggle( "fast",function(){
             jQuery('.searchbardiv').toggleClass( "moremargin" );
        } );

        jQuery('html,body').animate({
           scrollTop: $("#searchboxwrap").offset().top - 180
        });

        jQuery('#searchbox').focus();
        jQuery('#navbar nav').toggleClass('search-expanded');
        jQuery('.openclosesearch').toggle();
        jQuery('#navbar nav').animate({
           scrollTop: $(".search-expanded").offset().top
        });
    });

    jQuery( ".navbar-toggle" ).on( "click", function() {
        if (!jQuery(this).hasClass("collapsed")) {
            if (jQuery('.searchbardiv ').hasClass("moremargin")) {
            jQuery('#formsearch').slideToggle( "fast",function(){
                 jQuery( '.searchbardiv' ).toggleClass( "moremargin" );
            } );
            jQuery('.openclosesearch').toggle();
            }
            jQuery('html,body').removeClass('overflow-no-scroll-mobile');
        }else{
            jQuery('html,body').addClass('overflow-no-scroll-mobile');
        }
    });

    jQuery('.event-filters .dropdown-menu').on({
        "click":function(e){
          e.stopPropagation();
        }
    });

    // jQuery( ".main-nav-li" ).mouseover(function() {
    //     //jQuery('.main-nav-li').removeClass('open');
    //     jQuery(this).addClass('open');
    // });

    jQuery('#buttonsearch').mouseover(function() {
      jQuery('.main-nav-li').removeClass('open');
    });

    // close ad
    jQuery('#close-ad').click(function() {
        jQuery('.full-width-ad-wrapper').slideUp('300');
        jQuery('.full-page-ad-wrapper').slideUp('300');
    });


    jQuery( ".hvr-sweep-to-right" ).on( "click", function() {;
        jQuery(this).trigger('mouseenter');
    });

    // change location
    jQuery("#change-location-list").on('click', 'a', function(e){
        $("#change-location-list li a").removeClass('loc-selected');
        e.preventDefault();
        $(this).addClass('loc-selected');
        jQuery("#loc-phone").show();
        var currentLocation = $(this).text()
        jQuery("#selectedLocation").html(" " +currentLocation);
    });

    // fix scroll on mobile nav
    jQuery(".navbar-collapse").css({ maxHeight: jQuery(window).height() - jQuery(".navbar-header").height() + "px" });
});

