/*******************************
PROJECT : REC ROOM
JAVASCRIPT : HOME JS
to Initialize & modify behaviour
/*******************************/



    // jQuery('.grid').isotope({
    //     itemSelector: '.grid-item',
    //     masonry: {
    //         columnWidth: 100,
    //         gutter: 2.4,
    //     }
    // });

    // jQuery( '#dl-menu' ).dlmenu({
    //     animationClasses : { classin : 'dl-animate-in-2', classout : 'dl-animate-out-2' }
    // });
    // jQuery('#dl-menu-location').dlmenu();

$( function() {


  var $container = $('#container').masonry({
    itemSelector: '.grid-item',
          columnWidth: ".grid-sizer",
          gutter: ".gutter-sizer",
  });
// jQuery('.grid').isotope({
//     itemSelector: '.grid-item',
//     masonry: {
//         columnWidth: 100,
//         gutter: 2.4,
//     }
// });
//   $('#load-images').click( function() {
//     return triggerGetItem();
//   });

//     function triggerGetItem() {
//         var $items = getItems();
//         // hide by default
//         $items.hide();
//         // append to container
//         $container.append( $items );
//         $items.imagesLoaded().progress( function( imgLoad, image ) {
//           // get item
//           // image is imagesLoaded class, not <img>
//           // <img> is image.img
//           var $item = $( image.img ).parents('.item');
//           // un-hide item
//           $item.show();
//           // masonry does its thing
//           $container.masonry( 'appended', $item );
//         });
//     }
//     triggerGetItem();
// });

function randomInt( min, max ) {
  return Math.floor( Math.random() * max + min );
}

function getItem() {
  var width = randomInt( 400, 400 );
  var height = randomInt( 150, 250 );
  var item = '<div style="height:'+height+'px" class="item grid-item view view-third"><div class="box box-2 box-thm-1" >'+
      '<img  src="http://loremflickr.com/400/' + height + '/nightlife" /><div class="box-content-wrap mask"><div class="box-content box-content-nb"><h2>Hover Style</h2><a href="#" class="btn btn-wht btn-3 hvr-bounce-to-bottom">Read More</a></div></div>'+
      '</div></div>';
  return item;
}

function getItems() {
  var items = '';
  for ( var i=0; i < 12; i++ ) {
    items += getItem();
  }
  // return jQuery object
  return $( items );
}

});
